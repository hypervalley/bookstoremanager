package com.rodrigopeleias.bookstoremanager.books.builder;

import com.rodrigopeleias.bookstoremanager.books.dto.BookRequestDTO;
import com.rodrigopeleias.bookstoremanager.users.builder.UserDTOBuilder;
import com.rodrigopeleias.bookstoremanager.users.dto.UserDTO;

import lombok.Builder;

@Builder
public class BookRequestDTOBuilder {

    @Builder.Default
    private Long id = 1L;
    
    @Builder.Default
    private String name = "Spring Root Pro";
    
    @Builder.Default
    private String isbn = "978-3-16-148428-8";
    
    @Builder.Default
    private Long publisherId = 3L; 

    @Builder.Default
    private Long authorId = 2L;
   
    @Builder.Default
    private Integer pages = 200; 

    @Builder.Default
    private Integer chapters = 10; 
       
    @Builder.Default
    private final UserDTO userDTO = UserDTOBuilder.builder().build().buildUserDTO();
        
    public BookRequestDTO buildRequestBookDTO() {
        return new BookRequestDTO(
            id,
            name,
            isbn, 
            publisherId,
            authorId,
            pages,
            chapters
        );
        

    }
    
}
