package com.rodrigopeleias.bookstoremanager.users.controller;

import com.rodrigopeleias.bookstoremanager.users.builder.UserDTOBuilder;
//import com.rodrigopeleias.bookstoremanager.users.controller.UserController;
//import com.rodrigopeleias.bookstoremanager.users.dto.UserDTO;
import com.rodrigopeleias.bookstoremanager.users.service.UserService;
//import com.rodrigopeleias.bookstoremanager.utils.JsonConvertionUtils;
//import static com.rodrigopeleias.bookstoremanager.utils.JsonConvertionUtils.asJsonString;

import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
//import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

//import static com.rodrigopeleias.bookstoremanager.utils.JsonConvertionUtils.asJsonString;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

    private static final String USER_API_URL_PATH = "/api/v1/users";
    
    private MockMvc mockMvc;

    @Mock
    private UserService userService;
    
    @InjectMocks
    private UserController userController;
   

    private UserDTOBuilder userDTOBuilder;
            
    @BeforeEach
    void setUp() {
        userDTOBuilder = UserDTOBuilder.builder().build();
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
            .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
            .setViewResolvers((s, locale) -> new MappingJackson2JsonView())
            .build();
    }

}
