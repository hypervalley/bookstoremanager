package com.rodrigopeleias.bookstoremanager.users.builder;

import java.time.LocalDate;

import com.rodrigopeleias.bookstoremanager.users.dto.UserDTO;
import com.rodrigopeleias.bookstoremanager.users.enums.Gender;
import com.rodrigopeleias.bookstoremanager.users.enums.Role;

import lombok.Builder;

@Builder
public class UserDTOBuilder {    

    @Builder.Default
    private Long id = 1L;

    @Builder.Default
    private String name = "Engels";
    
    @Builder.Default
    private Integer age = 45;

    @Builder.Default
    private Gender gender = Gender.MALE;
        
    @Builder.Default
    private String email = "engels@sintopia.net";

    @Builder.Default
    private String username = "engels";

    @Builder.Default
    private String password = "123";

    @Builder.Default
    private LocalDate birthDate = LocalDate.of(1800, 1, 26);

    @Builder.Default
    private Role role = Role.USER;

    public UserDTO buildUserDTO() {
        return new UserDTO(
            id, 
            name, 
            age, 
            gender,            
            email,
            username,
            password,
            birthDate,
            role
        );
    }
}
