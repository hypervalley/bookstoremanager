package com.rodrigopeleias.bookstoremanager.publishers.builder;

import com.rodrigopeleias.bookstoremanager.publishers.dto.PublisherDTO;
import java.time.LocalDate;
import lombok.Builder;

@Builder
public class PublisherDTOBuilder {
    
    @Builder.Default
    private final Long id = 1L;
        
    @Builder.Default
    private final String name = "Utopia Editora";
    
    @Builder.Default
    private final String code = "10K21";

    private final LocalDate foundationDate = LocalDate.of(2020, 02, 12);


    public PublisherDTO buildPublisherDTO() {
        return new PublisherDTO(
            id, 
            name, 
            code, 
            foundationDate
        );
    }
    
}
