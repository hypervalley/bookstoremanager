package com.rodrigopeleias.bookstoremanager.publishers.controller;

import com.rodrigopeleias.bookstoremanager.publishers.builder.PublisherDTOBuilder;
//import com.rodrigopeleias.bookstoremanager.publishers.controller.PublisherController;
//import com.rodrigopeleias.bookstoremanager.publishers.dto.PublisherDTO;
import com.rodrigopeleias.bookstoremanager.publishers.service.PublisherService;
//import com.rodrigopeleias.bookstoremanager.utils.JsonConvertionUtils;
//import static com.rodrigopeleias.bookstoremanager.utils.JsonConvertionUtils.asJsonString;

import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
//import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

//import static com.rodrigopeleias.bookstoremanager.utils.JsonConvertionUtils.asJsonString;

@ExtendWith(MockitoExtension.class)
public class PublisherControllerTest {

    private static final String USER_API_URL_PATH = "/api/v1/users";
    
    private MockMvc mockMvc;

    @Mock
    private PublisherService publisherService;
    
    @InjectMocks
    private PublisherController publisherController;
   

    private PublisherDTOBuilder publisherDTOBuilder;
            
    @BeforeEach
    void setUp() {
        publisherDTOBuilder = publisherDTOBuilder.builder().build();
        mockMvc = MockMvcBuilders.standaloneSetup(publisherController)
            .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
            .setViewResolvers((s, locale) -> new MappingJackson2JsonView())
            .build();
    }

}
