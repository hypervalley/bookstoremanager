package com.rodrigopeleias.bookstoremanager.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping
public class HelloController {

	@ApiOperation(value = "Return an example hello world")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Success method return")
	}) 
	@GetMapping("/hello")
	public String hello() {
		System.out.println("hello"); // TODO With spring security doesn't arrive hear.
		return "Hello Bookstore Manager";
	}

}
