package com.rodrigopeleias.bookstoremanager.publishers.service;

import com.rodrigopeleias.bookstoremanager.publishers.dto.PublisherDTO;
import com.rodrigopeleias.bookstoremanager.publishers.entity.Publisher;
import com.rodrigopeleias.bookstoremanager.publishers.mapper.PublisherMapper;
import com.rodrigopeleias.bookstoremanager.publishers.repository.PublisherRepository;
import com.rodrigopeleias.bookstoremanager.publishers.exception.PublisherNotFoundException;
import com.rodrigopeleias.bookstoremanager.exception.PublisherAlreadyExistsException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PublisherService {
    
    private final static PublisherMapper publisherMapper = 
        PublisherMapper.INSTANCE;

    private PublisherRepository publisherRepository;
    
    @Autowired
    public PublisherService(PublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }

    public PublisherDTO create(PublisherDTO publisherDTO) {
        verifyIfExists(publisherDTO.getName(), publisherDTO.getCode());
        Publisher publisherToCreate = publisherMapper.toModel(publisherDTO);
        Publisher createdPublisher = publisherRepository.save(publisherToCreate);
        return publisherMapper.toDTO(createdPublisher);
    }

    public PublisherDTO findById(Long id) {
        PublisherDTO foundPublisher = verifyAndGetPublisher(id);
        return foundPublisher;
        //return  publisherMapper.toDTO(foundPublisher);
    }

    public PublisherDTO verifyAndGetPublisher(Long id){
        PublisherDTO foundPublisher = publisherRepository.findById(id)
            .map(publisherMapper::toDTO)
            .orElseThrow(() -> new PublisherNotFoundException(id));
        return foundPublisher;
    }

    public List<PublisherDTO> findAll() {
        return publisherRepository.findAll()
            .stream()
            .map(publisherMapper::toDTO)
            .collect(Collectors.toList());
    }
    
    public PublisherDTO findByName(String name) {
        Publisher foundPublisher = publisherRepository.findByName(name)
                .orElseThrow(() -> new PublisherNotFoundException(name));
        return publisherMapper.toDTO(foundPublisher);
    }

    private void verifyIfExists(String name, String code) {        
        Optional<Publisher> duplicatedPublisher = publisherRepository
            .findByNameOrCode(name, code);
        if(duplicatedPublisher.isPresent()) {
            throw new PublisherAlreadyExistsException(name, code);
        }
        //.ifPresent(publisher -> {throw new PublisherAlreadyExistsException(publisherName);});
    }

    private void verifyIfExists(Long id) {        
        publisherRepository.findById(id)
            .orElseThrow(() -> new PublisherNotFoundException(id));
    }

    public Publisher verifyAndGetIfExists(Long id) {
        return publisherRepository.findById(id)
            .orElseThrow(() -> new PublisherNotFoundException(id));
    }

    
    public void delete(Long id) {
        verifyAndGetPublisher(id);
        publisherRepository.deleteById(id);
    }
}