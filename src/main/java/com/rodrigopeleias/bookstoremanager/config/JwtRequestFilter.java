package com.rodrigopeleias.bookstoremanager.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.rodrigopeleias.bookstoremanager.users.service.AuthenticationService;
import com.rodrigopeleias.bookstoremanager.users.service.JwtTokenManager;

// Verify if token exists and add token to context.
@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    
    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private JwtTokenManager jwtTokenManager; 


    @Override
    protected void doFilterInternal(
        HttpServletRequest request,
        HttpServletResponse response,
        FilterChain chain
    ) throws ServletException, IOException {

        var username = "";
        var jwtToken = ""; 

        String requestTokenHeader = request.getHeader("authorization");
        if(isTokenPresent(requestTokenHeader)) {
            System.out.println("jwtRequestFilter isTokenPresent if");
            jwtToken = requestTokenHeader.substring(7);
            System.out.println("jwtToken");
            System.out.println(jwtToken);
            username = jwtTokenManager.getUsernameFromToken(jwtToken);            
            System.out.println("username");
            System.out.println(username);
        } else {
            System.out.println("jwtRequestFilter isTokenPresent else");
            logger.warn("Jwt token does not begin with Bearer string");
        };

        if(isUsernameInContext(username)) {
            addUsernameInContext(request, username,jwtToken);
        }
        chain.doFilter(request, response);
    }

    private boolean isTokenPresent(String requestTokenHeader) {
        System.out.println("jwtRequestFilter isTokenPresent");
        return requestTokenHeader != null && requestTokenHeader.startsWith("Bearer");
    }

    private void addUsernameInContext(HttpServletRequest request, String username, String jwtToken) {
        System.out.println("jwtRequestFilter addUsernameInContext");
        UserDetails userDetails = authenticationService.loadUserByUsername(username);
        if(jwtTokenManager.validateToken(jwtToken, userDetails)) {
            UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authenticationToken.setDetails(
                    new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
    }

    private boolean isUsernameInContext(String username) {
        System.out.println("jwtRequestFilter isUsernameInContext");
        //return username != null && SecurityContextHolder.getContext().getAuthentication() == null;
        System.out.println("Teste - username"); // TODO  Test
        System.out.println(username); // TODO Test
        return !username.isEmpty() && SecurityContextHolder.getContext().getAuthentication() == null;
    }


}
