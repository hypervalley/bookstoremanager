package com.rodrigopeleias.bookstoremanager.books.controller;

import java.util.List;

import com.rodrigopeleias.bookstoremanager.books.dto.BookRequestDTO;
import com.rodrigopeleias.bookstoremanager.books.dto.BookResponseDTO;
import com.rodrigopeleias.bookstoremanager.users.dto.AuthenticatedUser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api("Authors management")
public interface BookControllerDocs {


    @ApiOperation(value = "Book creation operation")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Success book creation"),
        @ApiResponse(code = 400, message = "Missing required fields, wrong field range value or book already registered on system ")
    })
    BookResponseDTO create(AuthenticatedUser authenticatedUser, BookRequestDTO bookRequestDTO);

    @ApiOperation(value = "Book find by id and user operation")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Success book creation"),
        @ApiResponse(code = 400, message = "Missing required fields, wrong field range value or book already registered on system ")
    })
    BookResponseDTO findByIdAndUser(AuthenticatedUser authenticatedUser, Long bookId);


    @ApiOperation(value = "List all books by authenticated user")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Book list found by authenticated user informed")
    })
	List<BookResponseDTO> findAllByUser(AuthenticatedUser authenticatedUser); 
	

    @ApiOperation(value = "Book delete operation")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Success author deleted"),
        @ApiResponse(code = 404, message = "Book not found error code")
    })
    void deleteByIdAndUser(AuthenticatedUser authenticatedUser, Long bookId);


    @ApiOperation(value = "Book bu user successfully updated")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Success book updated"),
        @ApiResponse(code = 400, message = "Missing reqired fields, wrong field range value or book already registered on system"),
        @ApiResponse(code = 404, message = "Book not found error")
    })
    BookResponseDTO updateByIdAndUser(AuthenticatedUser authenticatedUser, Long bookId, BookRequestDTO bookRequestDTO);

}
