package com.rodrigopeleias.bookstoremanager.books.entity;

import com.rodrigopeleias.bookstoremanager.author.entity.Author;
import com.rodrigopeleias.bookstoremanager.entity.Auditable;
import com.rodrigopeleias.bookstoremanager.publishers.entity.Publisher;
import com.rodrigopeleias.bookstoremanager.users.entity.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Book extends Auditable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false, length = 100)
    private String name;
    
    @Column(nullable = false)
    private String isbn;
    
    @Column(nullable = false, columnDefinition = "integer default 0")
    private int pages; 

    @Column(nullable = false, columnDefinition = "integer default 0")
    private int chapters; 
   
    @ManyToOne(cascade = {CascadeType.MERGE}) 
    private Author author;

    @ManyToOne(cascade = {CascadeType.MERGE}) 
    private Publisher publisher;

    @ManyToOne(cascade = {CascadeType.MERGE}) 
    private User user;

}