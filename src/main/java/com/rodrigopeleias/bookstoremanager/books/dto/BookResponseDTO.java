package com.rodrigopeleias.bookstoremanager.books.dto;

import com.rodrigopeleias.bookstoremanager.author.entity.Author;
import com.rodrigopeleias.bookstoremanager.publishers.entity.Publisher;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.validator.constraints.ISBN;

@Data
@AllArgsConstructor
@NoArgsConstructor 
public class BookResponseDTO {    
    
    private Long id;
    
    private String name;
    
    private String isbn;
    
    private Long authorId;

    private Long publisherId; 

    // private Author author;

    // private Publisher publisher; 
    
    private Integer pages; 

    private Integer chapters; 
    
    
        
}
