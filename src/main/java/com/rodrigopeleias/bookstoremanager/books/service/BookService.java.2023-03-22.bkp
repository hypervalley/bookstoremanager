package com.rodrigopeleias.bookstoremanager.books.service;

import com.rodrigopeleias.bookstoremanager.author.entity.Author;
import com.rodrigopeleias.bookstoremanager.author.service.AuthorService;
import com.rodrigopeleias.bookstoremanager.books.dto.BookRequestDTO;
import com.rodrigopeleias.bookstoremanager.books.dto.BookResponseDTO;
import com.rodrigopeleias.bookstoremanager.books.entity.Book;
import com.rodrigopeleias.bookstoremanager.books.exception.BookAlreadyExistsException;
import com.rodrigopeleias.bookstoremanager.books.mapper.BookMapper;
import com.rodrigopeleias.bookstoremanager.books.repository.BookRepository;
import com.rodrigopeleias.bookstoremanager.publishers.entity.Publisher;
//import com.rodrigopeleias.bookstoremanager.author.exception.BookNotFoundException;
//import com.rodrigopeleias.bookstoremanager.exception.BookAlreadyExistsException;
import com.rodrigopeleias.bookstoremanager.publishers.service.PublisherService;
import com.rodrigopeleias.bookstoremanager.users.dto.AuthenticatedUser;
import com.rodrigopeleias.bookstoremanager.users.entity.User;
import com.rodrigopeleias.bookstoremanager.users.exception.UserNotFoundException;
import com.rodrigopeleias.bookstoremanager.users.repository.UserRepository;
import com.rodrigopeleias.bookstoremanager.users.service.UserService;

import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class BookService {
    
    private final static BookMapper bookMapper = 
        BookMapper.INSTANCE;
    
    private BookRepository bookRepository;
        
    private UserService userService;

    private UserRepository userRepository;

    private AuthorService authorService;

    private PublisherService publisherService;
    
    public BookResponseDTO create(
        AuthenticatedUser authenticatedUser, 
        BookRequestDTO bookRequestDTO) {
        User foundAuthenticatedUser = userService.verifyAndGetIfExists(authenticatedUser.getUsername());

        System.out.println("antes do verifyIfBookIsAlreadyRegistered");
        verifyIfBookIsAlreadyRegistered(foundAuthenticatedUser, bookRequestDTO);
        System.out.println("após o verifyIfBookIsAlreadyRegistered");

        Author foundAuthor = authorService.verifyAndGetIfExists(bookRequestDTO.getAuthorId());
        Publisher foundPublisher = publisherService.verifyAndGetIfExists(bookRequestDTO.getPublisherId());
        

        System.out.println("antes do bookMapper");
        System.out.println("bookRequestDTO");
        System.out.println(bookRequestDTO);
        Book bookToSave = bookMapper.toModel(bookRequestDTO);
        bookToSave.setUser(foundAuthenticatedUser);
        bookToSave.setAuthor(foundAuthor);
        bookToSave.setPublisher(foundPublisher);

        System.out.println("antes do salvamento");
        System.out.println("bookToSave");
        //System.out.println(bookToSave);
        Book savedBook = bookRepository.save(bookToSave);
        System.out.println("após o salvamento");
        return bookMapper.toDTO(savedBook);

    }


    private User verifyAndGetUserIfExists(String username){
        return userRepository.findByUsername(username)
            .orElseThrow(() -> new UserNotFoundException(username));
    }

    private void verifyIfBookIsAlreadyRegistered(
        User foundUser,
        BookRequestDTO bookRequestDTO ) {
        bookRepository.findByNameAndIsbnAndUser(
            bookRequestDTO.getName(),
            bookRequestDTO.getIsbn(),
            foundUser)
            .ifPresent(duplicatedBook -> {
                throw new BookAlreadyExistsException(
                    bookRequestDTO.getName(),
                    bookRequestDTO.getIsbn(),
                    foundUser.getUsername());
        });
    }

/*
    public AuthorDTO create(AuthorDTO authorDTO) {
        verifyIfExists(authorDTO.getName());
        Author authorToCreate = authorMapper.toModel(authorDTO);
        Author createdAuthor = authorRepository.save(authorToCreate);
        return authorMapper.toDTO(createdAuthor);
    }

    public AuthorDTO findById(Long id) {
        Author foundAuthor = verifyAndGetAuthor(id);
        return  authorMapper.toDTO(foundAuthor);
    }

    public Author verifyAndGetAuthor(Long id){
        Author foundAuthor = authorRepository.findById(id)
            .orElseThrow(() -> new AuthorNotFoundException(id));
        return foundAuthor;    
    }

    public List<AuthorDTO> findAll() {
        return authorRepository.findAll()
            .stream()
            .map(authorMapper::toDTO)
            .collect(Collectors.toList());
    }
    
    public AuthorDTO findByName(String name) {
        Author foundAuthor = authorRepository.findByName(name)
                .orElseThrow(() -> new AuthorNotFoundException(name));
        return authorMapper.toDTO(foundAuthor);
    }

    private void verifyIfExists(String authorName) {
        authorRepository.findByName(authorName)
        .ifPresent(author -> {throw new AuthorAlreadyExistsException(authorName);});
    }

    public Author verifyAndGetIfExists(Long id) {
        return authorRepository.findById(id)
                .orElseThrow(() -> new AuthorNotFoundException(id));
    }

    
    public void delete(Long id) {
        verifyAndGetAuthor(id);
        authorRepository.deleteById(id);
    }
 */    

}