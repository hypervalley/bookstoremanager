package com.rodrigopeleias.bookstoremanager.books.service;

import com.rodrigopeleias.bookstoremanager.author.entity.Author;
import com.rodrigopeleias.bookstoremanager.author.service.AuthorService;
import com.rodrigopeleias.bookstoremanager.books.dto.BookRequestDTO;
import com.rodrigopeleias.bookstoremanager.books.dto.BookResponseDTO;
import com.rodrigopeleias.bookstoremanager.books.entity.Book;
import com.rodrigopeleias.bookstoremanager.books.exception.BookAlreadyExistsException;
import com.rodrigopeleias.bookstoremanager.books.mapper.BookMapper;
import com.rodrigopeleias.bookstoremanager.books.repository.BookRepository;
import com.rodrigopeleias.bookstoremanager.exception.BookNotFoundException;
import com.rodrigopeleias.bookstoremanager.publishers.entity.Publisher;
//import com.rodrigopeleias.bookstoremanager.author.exception.BookNotFoundException;
//import com.rodrigopeleias.bookstoremanager.exception.BookAlreadyExistsException;
import com.rodrigopeleias.bookstoremanager.publishers.service.PublisherService;
import com.rodrigopeleias.bookstoremanager.users.dto.AuthenticatedUser;
import com.rodrigopeleias.bookstoremanager.users.entity.User;
import com.rodrigopeleias.bookstoremanager.users.exception.UserNotFoundException;
import com.rodrigopeleias.bookstoremanager.users.repository.UserRepository;
import com.rodrigopeleias.bookstoremanager.users.service.UserService;

import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class BookService {
    
    private final static BookMapper bookMapper = 
        BookMapper.INSTANCE;
    
    private BookRepository bookRepository;
        
    private UserService userService;

    private UserRepository userRepository;

    private AuthorService authorService;

    private PublisherService publisherService;
    

    public BookResponseDTO create(
        AuthenticatedUser authenticatedUser, 
        BookRequestDTO bookRequestDTO) {
        User foundAuthenticatedUser = userService.verifyAndGetIfExists(authenticatedUser.getUsername());

        verifyIfBookIsAlreadyRegistered(foundAuthenticatedUser, bookRequestDTO);
        
        Author foundAuthor = authorService.verifyAndGetIfExists(bookRequestDTO.getAuthorId());
        Publisher foundPublisher = publisherService.verifyAndGetIfExists(bookRequestDTO.getPublisherId());
        
        Book bookToSave = bookMapper.toModel(bookRequestDTO);
        bookToSave.setUser(foundAuthenticatedUser);
        bookToSave.setAuthor(foundAuthor);
        bookToSave.setPublisher(foundPublisher);
        Book savedBook = bookRepository.save(bookToSave);

        return bookMapper.toDTO(savedBook);

    }


    public BookResponseDTO findByIdAndUser(AuthenticatedUser authenticatedUser, long bookId){
        User foundAuthenticatedUser = userService.verifyAndGetIfExists(authenticatedUser.getUsername());
        return bookRepository.findByIdAndUser(bookId, foundAuthenticatedUser)
            .map(bookMapper::toDTO)
            .orElseThrow(() -> new BookNotFoundException(bookId));

    }

    public List<BookResponseDTO> findAllByUser(AuthenticatedUser authenticatedUser){
        User foundAuthenticatedUser = userService.verifyAndGetIfExists(authenticatedUser.getUsername());
        return bookRepository.findAllByUser(foundAuthenticatedUser)
            .stream()
            .map(bookMapper::toDTO)
            .collect(Collectors.toList());

    }

    @Transactional // TODO - IMPORTANTE - only to delete. Why? 
    // Because this operation isn't read, transaction is necessary. And update?
    public void deleteByIdAndUser(AuthenticatedUser authenticatedUser, Long bookId) {
        User foundAuthenticatedUser = userService.verifyAndGetIfExists(authenticatedUser.getUsername());
        Book foundBookDelete = verifyAndGetifExists(bookId, foundAuthenticatedUser);
        bookRepository.deleteByIdAndUser(foundBookDelete.getId(), foundAuthenticatedUser);
    }

    public BookResponseDTO updateByIdAndUser(AuthenticatedUser authenticatedUser, Long bookId, BookRequestDTO bookRequestDTO) {
        
        
        User foundAuthenticatedUser = userService.verifyAndGetIfExists(authenticatedUser.getUsername());
        Book foundBook = verifyAndGetifExists(bookId, foundAuthenticatedUser);
        Author foundAuthor = authorService.verifyAndGetIfExists(bookRequestDTO.getAuthorId());
        Publisher foundPublisher = publisherService.verifyAndGetIfExists(bookRequestDTO.getPublisherId());
        
        Book bookToUpdate = bookMapper.toModel(bookRequestDTO);
        bookToUpdate.setId(foundBook.getId());
        bookToUpdate.setCreatedDate(foundBook.getCreatedDate());
        bookToUpdate.setUser(foundAuthenticatedUser);
        bookToUpdate.setAuthor(foundAuthor);
        bookToUpdate.setPublisher(foundPublisher);
        
        Book updatedBook = bookRepository.save(bookToUpdate);
        return bookMapper.toDTO(updatedBook);
    }


    private Book verifyAndGetifExists(Long bookId, User foundAuthenticatedUser) {
        return bookRepository.findByIdAndUser(bookId, foundAuthenticatedUser)
            .orElseThrow(() -> new BookNotFoundException(bookId));
    }






    /* 
    private User verifyAndGetUserIfExists(String username){
        return userRepository.findByUsername(username)
            .orElseThrow(() -> new UserNotFoundException(username));
    }
    */

    private void verifyIfBookIsAlreadyRegistered(
        User foundUser,
        BookRequestDTO bookRequestDTO ) {
        bookRepository.findByNameAndIsbnAndUser(
            bookRequestDTO.getName(),
            bookRequestDTO.getIsbn(),
            foundUser)
            .ifPresent(duplicatedBook -> {
                throw new BookAlreadyExistsException(
                    bookRequestDTO.getName(),
                    bookRequestDTO.getIsbn(),
                    foundUser.getUsername());
        });
    }

/*
    public AuthorDTO create(AuthorDTO authorDTO) {
        verifyIfExists(authorDTO.getName());
        Author authorToCreate = authorMapper.toModel(authorDTO);
        Author createdAuthor = authorRepository.save(authorToCreate);
        return authorMapper.toDTO(createdAuthor);
    }

    public AuthorDTO findById(Long id) {
        Author foundAuthor = verifyAndGetAuthor(id);
        return  authorMapper.toDTO(foundAuthor);
    }

    public Author verifyAndGetAuthor(Long id){
        Author foundAuthor = authorRepository.findById(id)
            .orElseThrow(() -> new AuthorNotFoundException(id));
        return foundAuthor;    
    }

    public List<AuthorDTO> findAll() {
        return authorRepository.findAll()
            .stream()
            .map(authorMapper::toDTO)
            .collect(Collectors.toList());
    }
    
    public AuthorDTO findByName(String name) {
        Author foundAuthor = authorRepository.findByName(name)
                .orElseThrow(() -> new AuthorNotFoundException(name));
        return authorMapper.toDTO(foundAuthor);
    }

    private void verifyIfExists(String authorName) {
        authorRepository.findByName(authorName)
        .ifPresent(author -> {throw new AuthorAlreadyExistsException(authorName);});
    }

    public Author verifyAndGetIfExists(Long id) {
        return authorRepository.findById(id)
                .orElseThrow(() -> new AuthorNotFoundException(id));
    }

    
    public void delete(Long id) {
        verifyAndGetAuthor(id);
        authorRepository.deleteById(id);
    }
 */    

}