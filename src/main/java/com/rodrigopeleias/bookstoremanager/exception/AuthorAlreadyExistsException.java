package com.rodrigopeleias.bookstoremanager.exception;

import javax.persistence.EntityExistsException;

// extends Throwable
public class AuthorAlreadyExistsException extends EntityExistsException {
    
    public AuthorAlreadyExistsException(String name){
        super(String.format("User with name %s already exists.", name));
    }
}
