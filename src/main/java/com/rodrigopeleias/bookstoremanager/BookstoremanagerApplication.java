package com.rodrigopeleias.bookstoremanager;

//import javax.servlet.annotation.WebServlet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.web.servlet.ServletRegistrationBean;
//import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableJpaRepositories
@EnableTransactionManagement
public class BookstoremanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookstoremanagerApplication.class, args);
	}	

	/* 
	@Bean
	public ServletRegistrationBean h2servletRegistration() {
    	ServletRegistrationBean registration = new ServletRegistrationBean(new WebServlet());
    	registration.addUrlMappings("/console/*");
    	return registration;
	}
	*/

}
