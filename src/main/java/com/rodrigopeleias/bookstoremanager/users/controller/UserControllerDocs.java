package com.rodrigopeleias.bookstoremanager.users.controller;

import java.util.List;

import com.rodrigopeleias.bookstoremanager.users.dto.JwtRequest;
import com.rodrigopeleias.bookstoremanager.users.dto.JwtResponse;
import com.rodrigopeleias.bookstoremanager.users.dto.MessageDTO;
import com.rodrigopeleias.bookstoremanager.users.dto.UserDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api("Users management")
public interface UserControllerDocs {
    
    @ApiOperation(value = "User creation operation")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Success user creation"),
        @ApiResponse(code = 400, message = "Missing required fields or an error on validadtion field rules")
    })
    MessageDTO create(UserDTO userDTO);

    @ApiOperation(value = "User exclusion operation")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Success user deleted"),
        @ApiResponse(code = 404, message = "User with informed id not found in the system")
    })
    void delete(Long id);

    @ApiOperation(value = "User update operation")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success user updated"),
        @ApiResponse(code = 400, message = "Missing required fields or an error on validadtion field rules")
    })
    MessageDTO update(Long id, UserDTO userToUpdateDTO); 

    @ApiOperation(value = "User authentication operation")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success user authenticated"),
        @ApiResponse(code = 404, message = "User not found")
    })
    JwtResponse createAuthenticationToken(JwtRequest jwtRequest);

/*
    @ApiOperation(value = "Find user by id operation")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success user found"),
        @ApiResponse(code = 404, message = "User not found error code")
    })
    UserDTO findById(Long id);

    @ApiOperation(value = "List all registered users")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Return all registered users"),        
    })
    List<UserDTO> findAll();

    
*/
}
