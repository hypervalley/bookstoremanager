package com.rodrigopeleias.bookstoremanager.users.service;

import com.rodrigopeleias.bookstoremanager.users.dto.MessageDTO;
import com.rodrigopeleias.bookstoremanager.users.dto.UserDTO;
import com.rodrigopeleias.bookstoremanager.users.entity.User;
import com.rodrigopeleias.bookstoremanager.users.exception.UserAlreadyExistsException;
import com.rodrigopeleias.bookstoremanager.users.exception.UserNotFoundException;
import com.rodrigopeleias.bookstoremanager.users.mapper.UserMapper;
import com.rodrigopeleias.bookstoremanager.users.repository.UserRepository;
//import com.rodrigopeleias.bookstoremanager.users.exception.UserNotFoundException;


import static com.rodrigopeleias.bookstoremanager.users.utils.MessageDTOUtils.creationMessage;
import static com.rodrigopeleias.bookstoremanager.users.utils.MessageDTOUtils.updatedMessage;

import java.time.LocalDate;
import java.time.LocalDateTime;
//import java.util.List;
import java.util.Optional;
//import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    
    private final static UserMapper userMapper = 
        UserMapper.INSTANCE;

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;
    
    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public MessageDTO create(UserDTO userToCreateDTO) {
        verifyIfExists(userToCreateDTO.getEmail(), userToCreateDTO.getUsername());

        User userToCreate = userMapper.toModel(userToCreateDTO);
        userToCreate.setPassword(passwordEncoder.encode(userToCreate.getPassword())); 
        User createdUser = userRepository.save(userToCreate);
        return creationMessage(createdUser);        
    }


    public void delete(Long id) {
        verifyAndGetIfExists(id);        
        userRepository.deleteById(id);
    }

    public MessageDTO update(Long id, UserDTO userToUpdateDTO) {
        User foundUser = verifyAndGetIfExists(id);

        //userToUpdateDTO.setId(foundUser.getId());
        userToUpdateDTO.setId(id);
        User userToUpdate = userMapper.toModel(userToUpdateDTO);
        
        userToUpdate.setCreatedDate(foundUser.getCreatedDate()); 
        userToUpdate.setCreatedBy(foundUser.getCreatedBy());
        userToUpdate.setLastModifiedDate(LocalDateTime.now()); //TODO Added. 
        userToUpdate.setPassword(passwordEncoder.encode(userToUpdate.getPassword())); 

        User updatedUser = userRepository.save(userToUpdate);
        return updatedMessage(updatedUser);        
    }

   
    
    private void verifyIfExists(String email, String username) {
        Optional<User> foundUser = userRepository.findByEmailOrUsername(email, username);
        if(foundUser.isPresent()){
            throw new UserAlreadyExistsException(email, username);
        }
    }

    private User verifyAndGetIfExists(Long id) {
        return userRepository.findById(id)
            .orElseThrow(() -> new UserNotFoundException(id));
    }

    public User verifyAndGetIfExists(String username) {
        return userRepository.findByUsername(username)
            .orElseThrow(() -> new UserNotFoundException(username));
    }

    /*
    public User verifyAndGetUserIfExists(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
    }
    */
/*
    public AuthorDTO findById(Long id) {
        Author foundAuthor = verifyAndGetAuthor(id);
        return  authorMapper.toDTO(foundAuthor);
    }

    public Author verifyAndGetAuthor(Long id){
        Author foundAuthor = authorRepository.findById(id)
            .orElseThrow(() -> new AuthorNotFoundException(id));
        return foundAuthor;    
    }

    public List<AuthorDTO> findAll() {
        return authorRepository.findAll()
            .stream()
            .map(authorMapper::toDTO)
            .collect(Collectors.toList());
    }
    
    public AuthorDTO findByName(String name) {
        Author foundAuthor = authorRepository.findByName(name)
                .orElseThrow(() -> new AuthorNotFoundException(name));
        return authorMapper.toDTO(foundAuthor);
    }

    private void verifyIfExists(String authorName) {
        authorRepository.findByName(authorName)
        .ifPresent(author -> {throw new AuthorAlreadyExistsException(authorName);});
    }

    public Author verifyAndGetIfExists(Long id) {
        return authorRepository.findById(id)
                .orElseThrow(() -> new AuthorNotFoundException(id));
    }

 */

}