package com.rodrigopeleias.bookstoremanager.users.repository;

import  com.rodrigopeleias.bookstoremanager.users.entity.User;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>{
    Optional<User> findByEmailOrUsername(String email, String username);

    Optional<User> findByUsername(String username);
}
