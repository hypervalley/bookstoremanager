package com.rodrigopeleias.bookstoremanager.author.controller;

import com.rodrigopeleias.bookstoremanager.author.controller.AuthorControllerDocs;
import com.rodrigopeleias.bookstoremanager.author.dto.AuthorDTO;
import com.rodrigopeleias.bookstoremanager.author.service.AuthorService;

import javax.validation.Valid;

import java.util.List;

import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


// TODO ViewResolver
@RestController
@RequestMapping("/api/v1/authors")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AuthorController implements AuthorControllerDocs {
    
    private final AuthorService authorService;

    /* 
    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }
    */

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)    
    public AuthorDTO create(@RequestBody @Valid AuthorDTO authorDTO) {
        System.out.println("0");
        System.out.println(authorDTO);
        System.out.println("authorDTO");
        return authorService.create(authorDTO);
    }

    @GetMapping("/{id}") // status default 200
    public AuthorDTO findById(@PathVariable Long id) {
        return authorService.findById(id);
    }

    @GetMapping
    public List<AuthorDTO> findAll() {
        return authorService.findAll();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        authorService.delete(id);
    }

}
